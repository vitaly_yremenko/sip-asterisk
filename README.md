
Поднимает asterisk в docker-контейнере с 2-мя пользователями

```
sip:1060@DOCKER_IP_ADDRESS
sip:1061@DOCKER_IP_ADDRESS
```

настройки для пользователей лежат в **./etc-asterisk/sip.conf**

Контейнер сконфигурирован на работу по протоколу wss (WebSocket + TLS)



#запускаем контейнер с астериском
```bash
    sudo docker run --rm --net=host --name test -v `pwd`/etc-asterisk:/etc/asterisk -v `pwd`/log-asterisk:/var/log/asterisk  respoke/asterisk
```




#запускаем HTTPS-сервер для проверки работы JSSIP


```bash
    http-server ./www -S --cert ./etc-asterisk/keys/asterisk.crt --key ./etc-asterisk/keys/asterisk.key
```
я использовал сервер [http-server](https://www.npmjs.com/package/http-server), можно использовать, любой другой
сертификаты используем те-же что и asterisk


#проверяем сертификат на порту

IP - подставить свой
8080 - https-server
8089 - asterisk wss-server

```bash
    openssl s_client -connect 192.168.8.119:8080
    openssl s_client -connect 192.168.8.119:8089
```



#Проверяем соединение по Websocket

```javascript
    conn = new WebSocket("wss://192.168.8.119:8089/ws", 'sip');
```


Проблемы: 

## При попытке отправит текстовое сообщение, астериск - ругается следующей ошибкой:

```bash
cheduling destruction of SIP dialog '565dkmbl17jcskj6gqf6' in 32000 ms (Method: MESSAGE)
[Aug 21 15:14:53] ERROR[20][C-00000001]: translate.c:1314 ast_translator_best_choice: Cannot determine best translation path since one capability supports no formats
[Aug 21 15:14:53] WARNING[20][C-00000001]: channel.c:6047 ast_request: No translator path exists for channel type SIP (native (g723|ulaw|alaw|gsm|g726|g726aal2|adpcm|slin|slin|slin|slin|slin|slin|slin|slin|slin|lpc10|g729|speex|speex|speex|ilbc|g722|siren7|siren14|testlaw|g719|opus|silk|silk|silk|silk)) to (none)
[Aug 21 15:14:53] WARNING[20][C-00000001]: app_dial.c:2437 dial_exec_full: Unable to create channel of type 'SIP' (cause 58 - Bearer capability not available)
```


## При попытке позвонить, Ошибок вроде нет, но к аддресату звонок не приходит. Возможно ошибка в конфигурации астериска. 

```bash

<--- SIP read from WS:192.168.8.119:59718 --->
INVITE sip:1061@192.168.8.119 SIP/2.0
Via: SIP/2.0/WSS u2lbdc1k8sbc.invalid;branch=z9hG4bK2208088
Max-Forwards: 69
To: <sip:1061@192.168.8.119>
From: <sip:1060@192.168.8.119>;tag=trnq2romck
Call-ID: 565dkjncvt2kcm801mt6
CSeq: 6838 INVITE
Authorization: Digest algorithm=MD5, username="1060", realm="192.168.8.119", nonce="5ddceb5c", uri="sip:1061@192.168.8.119", response="a8ee0caab045665782d37946d1ec7f13"
Contact: <sip:b0vpsdgs@u2lbdc1k8sbc.invalid;transport=ws;ob>
Content-Type: application/sdp
Session-Expires: 90
Allow: INVITE,ACK,CANCEL,BYE,UPDATE,MESSAGE,OPTIONS,REFER,INFO
Supported: timer,ice,replaces,outbound
User-Agent: JsSIP 3.0.13
Content-Length: 1657

v=0
o=- 666740028792916018 2 IN IP4 127.0.0.1
s=-
t=0 0
a=group:BUNDLE audio
a=msid-semantic: WMS CnxGCB0z1AR06UwMlgQd4iCJqpo8swTQPcSj
m=audio 39309 UDP/TLS/RTP/SAVPF 111 103 104 9 0 8 106 105 13 110 112 113 126
c=IN IP4 10.8.0.174
a=rtcp:9 IN IP4 0.0.0.0
a=candidate:4120782276 1 udp 2122260223 10.8.0.174 39309 typ host generation 0 network-id 2
a=candidate:3967956297 1 udp 2122194687 192.168.8.119 39889 typ host generation 0 network-id 1
a=candidate:3139295540 1 tcp 1518280447 10.8.0.174 9 typ host tcptype active generation 0 network-id 2
a=candidate:2718026169 1 tcp 1518214911 192.168.8.119 9 typ host tcptype active generation 0 network-id 1
a=ice-ufrag:0zai
a=ice-pwd:VyE7pPokY/unE00/4QojmnRp
a=ice-options:trickle
a=fingerprint:sha-256 7F:0E:D0:F2:6A:4F:4E:FC:04:08:A6:3F:A4:0F:6D:11:8A:82:7A:E5:6E:BC:39:E4:93:15:0D:B1:D8:F7:59:83
a=setup:actpass
a=mid:audio
a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
a=sendrecv
a=rtcp-mux
a=rtpmap:111 opus/48000/2
a=rtcp-fb:111 transport-cc
a=fmtp:111 minptime=10;useinbandfec=1
a=rtpmap:103 ISAC/16000
a=rtpmap:104 ISAC/32000
a=rtpmap:9 G722/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:8 PCMA/8000
a=rtpmap:106 CN/32000
a=rtpmap:105 CN/16000
a=rtpmap:13 CN/8000
a=rtpmap:110 telephone-event/48000
a=rtpmap:112 telephone-event/32000
a=rtpmap:113 telephone-event/16000
a=rtpmap:126 telephone-event/8000
a=ssrc:1763351674 cname:P8dze9qAoKYICYAu
a=ssrc:1763351674 msid:CnxGCB0z1AR06UwMlgQd4iCJqpo8swTQPcSj 22d881a9-4cee-4119-8e01-ad8aca36d5cf
a=ssrc:1763351674 mslabel:CnxGCB0z1AR06UwMlgQd4iCJqpo8swTQPcSj
a=ssrc:1763351674 label:22d881a9-4cee-4119-8e01-ad8aca36d5cf
<------------->
--- (15 headers 41 lines) ---
Using INVITE request as basis request - 565dkjncvt2kcm801mt6
Found peer '1060' for '1060' from 192.168.8.119:59718
Found RTP audio format 111
Found RTP audio format 103
Found RTP audio format 104
Found RTP audio format 9
Found RTP audio format 0
Found RTP audio format 8
Found RTP audio format 106
Found RTP audio format 105
Found RTP audio format 13
Found RTP audio format 110
Found RTP audio format 112
Found RTP audio format 113
Found RTP audio format 126
Found audio description format opus for ID 111
Found unknown media description format ISAC for ID 103
Found unknown media description format ISAC for ID 104
Found audio description format G722 for ID 9
Found audio description format PCMU for ID 0
Found audio description format PCMA for ID 8
Found unknown media description format CN for ID 106
Found unknown media description format CN for ID 105
Found audio description format CN for ID 13
Found unknown media description format telephone-event for ID 110
Found unknown media description format telephone-event for ID 112
Found unknown media description format telephone-event for ID 113
Found audio description format telephone-event for ID 126
Capabilities: us - (gsm|alaw|ulaw), peer - audio=(ulaw|alaw|g722|opus)/video=(nothing)/text=(nothing), combined - (alaw|ulaw)
Non-codec capabilities (dtmf): us - 0x1 (telephone-event|), peer - 0x3 (telephone-event|CN|), combined - 0x1 (telephone-event|)
Peer audio RTP is at port 10.8.0.174:39309
Looking for 1061 in from-internal (domain 192.168.8.119)
sip_route_dump: route/path hop: <sip:b0vpsdgs@u2lbdc1k8sbc.invalid;transport=ws;ob>

<--- Transmitting (no NAT) to 192.168.8.119:5060 --->
SIP/2.0 100 Trying
Via: SIP/2.0/WSS u2lbdc1k8sbc.invalid;branch=z9hG4bK2208088;received=192.168.8.119
From: <sip:1060@192.168.8.119>;tag=trnq2romck
To: <sip:1061@192.168.8.119>
Call-ID: 565dkjncvt2kcm801mt6
CSeq: 6838 INVITE
Server: Asterisk PBX 14.0.2
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, SUBSCRIBE, NOTIFY, INFO, PUBLISH, MESSAGE
Supported: replaces, timer
Session-Expires: 90;refresher=uas
Contact: <sip:1061@192.168.8.119:5060;transport=WS>
Content-Length: 0


<------------>
Audio is at 16966
Adding codec alaw to SDP
Adding codec gsm to SDP
Adding codec ulaw to SDP
Adding non-codec 0x1 (telephone-event) to SDP
Reliably Transmitting (NAT) to 192.168.8.119:35242:
INVITE sip:1cgisfh3@pul8i27bebhi.invalid;transport=ws SIP/2.0
Via: SIP/2.0/WS 192.168.8.119:5060;branch=z9hG4bK17ec1c89;rport
Max-Forwards: 70
From: <sip:1060@192.168.8.119>;tag=as31f37ac1
To: <sip:1cgisfh3@pul8i27bebhi.invalid;transport=ws>
Contact: <sip:1060@192.168.8.119:5060;transport=WS>
Call-ID: 7547bd201f2ffd89114b466b39797025@192.168.8.119:5060
CSeq: 102 INVITE
User-Agent: Asterisk PBX 14.0.2
Date: Mon, 21 Aug 2017 15:17:20 GMT
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, SUBSCRIBE, NOTIFY, INFO, PUBLISH, MESSAGE
Supported: replaces, timer
Content-Type: application/sdp
Content-Length: 1152

v=0
o=root 765436098 765436098 IN IP4 192.168.8.119
s=Asterisk PBX 14.0.2
c=IN IP4 192.168.8.119
t=0 0
m=audio 16966 RTP/SAVPF 8 3 0 101
a=rtpmap:8 PCMA/8000
a=rtpmap:3 GSM/8000
a=rtpmap:0 PCMU/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-16
a=ptime:20
a=maxptime:150
a=ice-ufrag:65411e7771c7237d450854d1043d7eb9
a=ice-pwd:4cb46bfe449f28c53da420ad53aaff69
a=candidate:Hc0a80877 1 UDP 2130706431 192.168.8.119 16966 typ host
a=candidate:Hac110001 1 UDP 2130706431 172.17.0.1 16966 typ host
a=candidate:Ha0800ae 1 UDP 2130706431 10.8.0.174 16966 typ host
a=candidate:S9fe0e97b 1 UDP 1694498815 159.224.233.123 17889 typ srflx raddr 192.168.8.119 rport 16966
a=candidate:Hc0a80877 2 UDP 2130706430 192.168.8.119 16967 typ host
a=candidate:Hac110001 2 UDP 2130706430 172.17.0.1 16967 typ host
a=candidate:Ha0800ae 2 UDP 2130706430 10.8.0.174 16967 typ host
a=candidate:S9fe0e97b 2 UDP 1694498814 159.224.233.123 17921 typ srflx raddr 192.168.8.119 rport 16967
a=connection:new
a=setup:actpass
a=fingerprint:SHA-256 6C:9A:E0:7E:5D:81:AF:57:FE:BF:34:0C:40:76:0B:61:D2:AF:6F:76:4A:83:FD:8F:CC:3A:87:7A:4B:70:5B:48
a=sendrecv

---
Scheduling destruction of SIP dialog '44e089fa71275a6106d34a0b7e466f1d@192.168.8.119:5060' in 32000 ms (Method: INVITE)
Scheduling destruction of SIP dialog '44e089fa71275a6106d34a0b7e466f1d@192.168.8.119:5060' in 32000 ms (Method: INVITE)

<--- Reliably Transmitting (no NAT) to 192.168.8.119:5060 --->
SIP/2.0 503 Service Unavailable
Via: SIP/2.0/WSS u2lbdc1k8sbc.invalid;branch=z9hG4bK2663979;received=192.168.8.119
From: <sip:1060@192.168.8.119>;tag=gaaf722m04
To: <sip:1061@192.168.8.119>;tag=as202a12ca
Call-ID: 565dkm6bvpbaaln20jmd
CSeq: 1418 INVITE
Server: Asterisk PBX 14.0.2
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, SUBSCRIBE, NOTIFY, INFO, PUBLISH, MESSAGE
Supported: replaces, timer
Content-Length: 0


<------------>

<--- SIP read from WS:192.168.8.119:59718 --->
ACK sip:1061@192.168.8.119 SIP/2.0
Via: SIP/2.0/WSS u2lbdc1k8sbc.invalid;branch=z9hG4bK2663979
To: <sip:1061@192.168.8.119>;tag=as202a12ca
From: <sip:1060@192.168.8.119>;tag=gaaf722m04
Call-ID: 565dkm6bvpbaaln20jmd
CSeq: 1418 ACK
Content-Length: 0

<------------->
--- (7 headers 0 lines) ---
Really destroying SIP dialog '565dkm6bvpbaaln20jmd' Method: INVITE
Really destroying SIP dialog '0u0g4frgq91gc6i0bfcj08' Method: REGISTER
Really destroying SIP dialog '3t1j854s2t7hoctgbhacmg' Method: REGISTER
Scheduling destruction of SIP dialog '7547bd201f2ffd89114b466b39797025@192.168.8.119:5060' in 32000 ms (Method: INVITE)
Scheduling destruction of SIP dialog '7547bd201f2ffd89114b466b39797025@192.168.8.119:5060' in 32000 ms (Method: INVITE)

<--- Reliably Transmitting (no NAT) to 192.168.8.119:5060 --->
SIP/2.0 503 Service Unavailable
Via: SIP/2.0/WSS u2lbdc1k8sbc.invalid;branch=z9hG4bK2208088;received=192.168.8.119
From: <sip:1060@192.168.8.119>;tag=trnq2romck
To: <sip:1061@192.168.8.119>;tag=as7ea0d766
Call-ID: 565dkjncvt2kcm801mt6
CSeq: 6838 INVITE
Server: Asterisk PBX 14.0.2
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, SUBSCRIBE, NOTIFY, INFO, PUBLISH, MESSAGE
Supported: replaces, timer
Content-Length: 0


<------------>

<--- SIP read from WS:192.168.8.119:59718 --->
ACK sip:1061@192.168.8.119 SIP/2.0
Via: SIP/2.0/WSS u2lbdc1k8sbc.invalid;branch=z9hG4bK2208088
To: <sip:1061@192.168.8.119>;tag=as7ea0d766
From: <sip:1060@192.168.8.119>;tag=trnq2romck
Call-ID: 565dkjncvt2kcm801mt6
CSeq: 6838 ACK
Content-Length: 0



```
