



function connect() {

  var user = document.getElementById('user').value;
  var uri = document.getElementById('uri').value;
  var password = document.getElementById('password').value;
  var ip = (/([1-9.]+)/).exec(uri)[0];
  var u = user+ '@' + ip

  var socket = new JsSIP.WebSocketInterface(uri);
  var configuration = {
    sockets  : [ socket ],
    uri      : user+ '@' + ip,
    password : password
  };

  console.log( 'configuration',configuration );

  window.coolPhone = new JsSIP.UA(configuration);


  coolPhone.on('connected', function(e){ console.log('CONNECTED',e) });

  coolPhone.on('disconnected', function(e){ console.log('DISCONNECTED',e)});

  coolPhone.on('newRTCSession', function(e){ console.log('newRTCSession',e) });

  coolPhone.on('newMessage', function(e){console.log('newMessage',e) });

  coolPhone.on('registered', function(e){ console.log('registered',e) });
  coolPhone.on('unregistered', function(e){ console.log('unregistered',e) });
  coolPhone.on('registrationFailed', function(e){ console.log('registrationFailed',e) });

  
  coolPhone.on('registered', function(e){
    document.getElementById('more').className = '';
    console.log('registered',e)
  });
  


  coolPhone.start();

}

document.getElementById('connect').onclick = connect

document.getElementById('send-message').onclick = function(){

  var user = document.getElementById('user2').value;
  var message = document.getElementById('message').value;

  var eventHandlers = {
      'succeeded': function(e){ console.log('succeeded',e); },
        'failed':    function(e){ console.log('failed',e); }
  };

  var options = {
    'eventHandlers': eventHandlers
  };
  
  coolPhone.sendMessage(user, message, options);

}

document.getElementById('call').onclick = function(){

  var user = document.getElementById('user2').value;
  var message = document.getElementById('message').value;

  var eventHandlers = {
      'succeeded': function(e){ console.log('succeeded',e); },
      'failed':    function(e){ console.log('failed',e); },
      'progress':   function(e){ console.log('progress',e);  },
      'confirmed':  function(e){ console.log('confirmed',e);  },
      'ended':      function(e){ console.log('ended',e);  }
  };

  var options = {
    'eventHandlers': eventHandlers
  };

  coolPhone.call(user, {
    mediaConstraints: {
    audio: true, // only audio calls
    video: false,    
  },
  'eventHandlers': eventHandlers
})

  
  

}